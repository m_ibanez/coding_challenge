import React, { useCallback, useState } from 'react';
import { useEffect } from 'react';
import { Grid } from "@material-ui/core";
import generateMessage, { Message } from './Api';
import MessageColumn from './components/MessageColumn';
import ErrorSnackbar from './components/ErrorSnackbar';
import AppTitle from './components/AppTitle';

const App: React.FC<{}> = () => {
  const [messages, setMessages] = useState<Message[]>([]);
  const [currentError, setCurrentError] = useState<Message|undefined>(undefined);
  const [streamingMessages, setStreamingMessages] = useState<boolean>(true);

  useEffect(() => {
    const cleanUp = generateMessage((message: Message) => {
      if (!streamingMessages) return;
      setMessages(oldMessages => [...oldMessages, message]);
      if (message.priority === 0)
        setCurrentError(message);
    });
    return cleanUp;
  }, [setMessages, setCurrentError, streamingMessages]);



  const errors = messages.filter(msg => msg.priority === 0).reverse();
  const warnings = messages.filter(msg => msg.priority === 1).reverse();
  const infos = messages.filter(msg => msg.priority === 2).reverse();

  const handleDismissMessage = useCallback((msg: Message) => {
    setMessages(oldMessages => oldMessages.filter(oldMsg => oldMsg !== msg));
  }, []);

  const handleClearMessages = () => {
    setMessages([]);
  };

  const handleToggleStream = () => {
    setStreamingMessages(!streamingMessages);
  };

  return (
    <div>
      <ErrorSnackbar message={currentError} />
      <AppTitle streamMessages={streamingMessages} toggleStream={handleToggleStream} onClearMessages={handleClearMessages} />
      <Grid container alignContent="space-between" style={{ padding: "0 100px" }} >
        <MessageColumn title="Error Type 1" messages={errors} onDismiss={handleDismissMessage} />
        <MessageColumn title="Warning Type 2" messages={warnings} onDismiss={handleDismissMessage} />
        <MessageColumn title="Info Type 3" messages={infos} onDismiss={handleDismissMessage} />
      </Grid>
    </div>
  );
}

export default App;
