import React, { FC, useState, useEffect } from "react";
import { Button, Snackbar } from "@material-ui/core";
import { Message } from "../Api";

interface ErrorSnackbarProps {
    message?: Message,
}

const ErrorSnackbar: FC<ErrorSnackbarProps> = ({ message }) => {
    const [open, setOpen] = useState(false);

    useEffect(() => {
        if (!message) return;
        setOpen(false);
        setTimeout(() => {
            setOpen(true);
        }, 500)
    }, [message]);

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <Snackbar
            open={open}
            message={message?.message}
            autoHideDuration={2000}
            anchorOrigin={{
                horizontal: "right",
                vertical: "top",
            }}
            action={
                <Button
                    color="secondary"
                    variant="text"
                    onClick={handleClose}
                >
                    Close
                </Button>
            }
        />
    );
}

export default ErrorSnackbar;
