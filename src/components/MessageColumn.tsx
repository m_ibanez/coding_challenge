import React, { FC } from "react";
import { Message } from "../Api";
import { Grid, Typography } from "@material-ui/core";
import MessageCard from "./MessageCard";

interface MessageColumnProps {
    title?: string,
    messages: Message[],
    onDismiss: (msg: Message) => void,
}

const MessageColumn: FC<MessageColumnProps> = ({ title, messages, onDismiss }) => {
    return (
        <Grid item xs={12} md={4} lg={4} style={{ padding: 5 }} >
            <Typography variant="title" >{title}</Typography>
            <Typography>Count {messages?.length}</Typography>
            {messages?.map(msg => <MessageCard key={msg?.message} message={msg} onDismiss={() => onDismiss(msg)} /> )}
        </Grid>
    );
}

export default MessageColumn;
