import React, { FC } from "react";
import {
    Button,
    Divider,
    Typography,
} from "@material-ui/core";

interface AppTitleProps {
    streamMessages?: boolean,
    toggleStream?: (ev: React.SyntheticEvent) => void,
    onClearMessages?: (ev: React.SyntheticEvent) => void,
}

const AppTitle: FC<AppTitleProps> = ({ streamMessages, toggleStream, onClearMessages }) => {
    return (
        <div style={{ marginBottom: 20 }} >
            <Typography variant="h5" >nuffsaid.com Coding Challenge</Typography>
            <Divider />
            <div 
                style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    padding: "5px 44.5%",
                }}
            >
                <Button
                    style={{ background: "#88FCA3" }}
                    variant="contained"
                    onClick={toggleStream}
                >
                    {streamMessages ? "Stop" : "Start"}
                </Button>
                <Button
                    style={{ background: "#88FCA3" }}
                    variant="contained"
                    onClick={onClearMessages}
                >
                    Clear
                </Button>
            </div>
        </div>
    );
}

export default AppTitle;
