import React from "react";
import { Message } from "../Api";
import {
    Button,
    Card,
    CardActions,
    CardContent,
    Typography,
} from "@material-ui/core";

const styles = [
    {
        background: "#F56236",
        marginBottom: 10,
    },
    {
        background: "#FCE788",
        marginBottom: 10,
    },
    {
        background: "#88FCA3",
        marginBottom: 10,
    },
];

interface MessageCardProps {
    message: Message,
    onDismiss: (ev: React.SyntheticEvent) => void,
}

const MessageCard: React.FC<MessageCardProps> = ({ message, onDismiss }) => {
    return (
        <Card style={styles[message.priority]} >
            <CardContent>
                <Typography variant="caption" >{message?.message}</Typography>
            </CardContent>
            <CardActions disableActionSpacing style={{ justifyContent: "flex-end" }} >
                <Button variant="text" onClick={onDismiss} style={{ textTransform: "none" }}>
                    <Typography variant="caption" >Clear</Typography>
                </Button>
            </CardActions>
        </Card>
    );
}

export default React.memo(MessageCard);
