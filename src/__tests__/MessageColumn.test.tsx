import { render, fireEvent } from "@testing-library/react";
import { Message } from "../Api";
import MessageColumn from "../components/MessageColumn";

jest.mock("../components/MessageCard", () => {
    return (props: { message: Message, onDismiss: () => {} }) => (
        <div>
            <span>{props.message.message}</span>
            <button onClick={props.onDismiss} data-testid={props.message.message}>Clear</button>
        </div>
    )
})

describe("<MessageColumn />", () => {

    const messages = ["Message One", "Message Two"].map(message => ({
        priority: 2,
        message,
    }))

    it("should render title", async () => {
        const expectedTitle = "Column message";
        const { findByText } = render(<MessageColumn title={expectedTitle} messages={[]} onDismiss={() => {}} />)
        const title = await findByText(expectedTitle);
        expect(title).toBeInTheDocument();
    });

    it("should render list items", async () => {
        const { findByText } = render(<MessageColumn messages={messages} onDismiss={() => {}} />)
        const counter = await findByText(/Count 2/g);
        expect(counter).toBeInTheDocument();
        for (const index in messages) {
            const msg = messages[index];
            const text = await findByText(msg.message);
            expect(text).toBeInTheDocument();
        }
    });

    it("should call dismiss function when clear button is pressed", async () => {
        const dismissFunction = jest.fn();
        const { findByTestId } = render(<MessageColumn messages={messages} onDismiss={dismissFunction} />)
        for (const index in messages) {
            const msg = messages[index];
            const button = await findByTestId(msg.message);
            expect(button).toBeInTheDocument();
            fireEvent.click(button);
            expect(dismissFunction).toHaveBeenCalledWith(msg);
        }
    });
})
