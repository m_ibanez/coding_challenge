import { render, act } from '@testing-library/react';
import { Message } from '../Api';
import App from '../App';

jest.mock("../components/MessageColumn", () => {
  return (props: { title: string, messages: [Message], onDismiss: () => void }) => (
    <div data-testid={props.title}>
      <div>{props.title}</div>
      {props.messages.map(msg => (
         <div key={msg.message}>
           <span>{msg.message}</span>
          <button onClick={props.onDismiss}>Clear</button>
         </div>
      ))}
    </div>
  );
});

const events: any[] = [];

const sender = {
  send: (msg: Message) => {
    while (events.length > 0) {
      const currentEvent = events.shift();
      if (currentEvent) currentEvent(msg);
    }
  },
  receive: (callback: (msg: Message) => void) => {
    events.push(callback)
  },
};

const messageStreamMock = (cb: (msg: Message) => void) => {
  sender.receive(cb);
  return () => {};
}

jest.mock("../Api.ts", () => messageStreamMock);

describe("<App />", () => {
  test('renders three columns', async () => {
    const { findByText } = render(<App />);
    const titles = [
      "Error Type 1",
      "Warning Type 2",
      "Info Type 3",
    ];
    for (const index in titles) {
      const currentTitle = titles[index];
      const title = await findByText(currentTitle);
      expect(title).toBeInTheDocument();
    }
  });

  it("renders message received from api", async () => {
    const { findByText } = render(<App />)
    const expectedMessage = "Should render message"
    act(() => {
      sender.send({ message: expectedMessage, priority: 0 });
    });
    const message = await findByText(expectedMessage);
    expect(message).toBeInTheDocument();
  });
})
