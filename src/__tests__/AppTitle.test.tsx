import { render, fireEvent } from "@testing-library/react";
import AppTitle from "../components/AppTitle";

describe("<AppTitle />", () => {
    it("should render app title", async () => {
        const { findByText } = render(<AppTitle />);
        const title = await findByText("nuffsaid.com Coding Challenge");
        expect(title).toBeInTheDocument();
    });

    it("should render stop button when messages are streaming", async () => {
        const toggleFunction = jest.fn();
        const { findByText } = render(<AppTitle streamMessages={true} toggleStream={toggleFunction} />);
        const stopButton = await findByText("Stop");
        expect(stopButton).toBeInTheDocument();
        fireEvent.click(stopButton);
        expect(toggleFunction).toHaveBeenCalledTimes(1);
    });

    it("should render start button when messages are not streaming", async () => {
        const toggleFunction = jest.fn();
        const { findByText } = render(<AppTitle streamMessages={false} toggleStream={toggleFunction} />);
        const startButton = await findByText("Start");
        expect(startButton).toBeInTheDocument();
        fireEvent.click(startButton);
        expect(toggleFunction).toHaveBeenCalledTimes(1);
    });

    it("should call event when clear button is pressed", async () => {
        const clearFunction = jest.fn();
        const { findByText } = render(<AppTitle onClearMessages={clearFunction} />);
        const clearButton = await findByText("Clear");
        expect(clearButton).toBeInTheDocument();
        fireEvent.click(clearButton);
        expect(clearFunction).toHaveBeenCalledTimes(1);
    });
});
