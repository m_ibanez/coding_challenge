import { render, fireEvent } from "@testing-library/react"
import MessageCard from "../components/MessageCard";
import { Message } from "../Api";

describe("<MessageCard />", () => {

    const testMessage = {
        message: "", 
        priority: 2,
    };

    it("should display message button when message is not empty", async () => {
        const expectedMessage = "Message To Display";
        testMessage.message = expectedMessage;
        const { findByText } = renderCard(testMessage);
        const clearButton = await findByText(expectedMessage);
        expect(clearButton).toBeInTheDocument();
    });

    it("should call dismiss function when button is pressed", async () => {
        const expectedAction = jest.fn();
        const { findByText } = renderCard(testMessage, expectedAction);
        const clearButton = await findByText("Clear");
        expect(clearButton).toBeInTheDocument();
        fireEvent.click(clearButton);
        expect(expectedAction).toBeCalledTimes(1);
    });
});

function renderCard(message: Message, onDismiss?: () => void) {
    if (!onDismiss)
        onDismiss = () => {};
    return render(<MessageCard message={message} onDismiss={onDismiss} />);
}
