import { render, fireEvent } from "@testing-library/react";
import ErrorSnackbar from "../components/ErrorSnackbar";

describe("<ErrorSnackbar />", () => {

    const testMessage = {
        message: "Test Message",
        priority: 0,
    };

    it("should not render when message is undefined", () => {
        const { queryByText } = render(<ErrorSnackbar message={undefined} />);
        const snack = queryByText(testMessage.message);
        expect(snack).not.toBeInTheDocument();
    });

    it("should render when message is defined", async () => {
        const { findByText } = render(<ErrorSnackbar message={testMessage} />);
        const snack = await findByText(testMessage.message);
        expect(snack).toBeInTheDocument();
    });

    it("should dismiss after three seconds", () => {
        const { queryByText } = render(<ErrorSnackbar message={testMessage} />);
        setTimeout(() => {
            const snack = queryByText(testMessage.message);
            expect(snack).not.toBeInTheDocument();
        }, 3000);
    });

    it("should dismiss after dismiss button is pressed", () => {
        const { findByText, queryByText } = render(<ErrorSnackbar message={testMessage} />);
        setTimeout(async () => {
            const dismissButton = await findByText("Close");
            fireEvent.click(dismissButton);
            const snack = queryByText(testMessage.message);
            expect(snack).not.toBeInTheDocument();
        }, 500);
    });
});
